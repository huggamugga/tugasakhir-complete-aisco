module aisco.financialreport.core {
    exports aisco.financialreport.core;
    exports aisco.financialreport;
    requires vmj.routing.route;
    requires vmj.object.mapper;
    requires java.logging;
    requires aisco.program.core;
}