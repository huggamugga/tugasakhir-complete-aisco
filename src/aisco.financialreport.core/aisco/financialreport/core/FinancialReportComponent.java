package aisco.financialreport.core;

import java.util.*;

import aisco.program.core.ProgramComponent;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

import aisco.program.core.Program;

public abstract class FinancialReportComponent implements FinancialReport {
    private VMJDatabaseUtil vmjDBUtil;

    @VMJDatabaseField(primaryKey = true)
    public int id;

    public String dateStamp;

    public int amount;

    public String description;

    @VMJDatabaseField(foreignTableName = "program_core", foreignColumnName = "id")
    public Program idProgram;

    public String idCoa;

    public FinancialReportComponent() {
        this.vmjDBUtil = new VMJDatabaseUtil();
    }

    @Route(url="getDescription")
    public String getDescription(VMJExchange vmjExchange) {
        Object id = vmjExchange.getGETParam("id");
        ArrayList<String> requiredFields = new ArrayList<>();
        requiredFields.add("description");

        HashMap<String, Object> hasil = vmjDBUtil.getDataById("financialreport_core", requiredFields, id.toString());

        return hasil.get("description").toString();
    }

    @Route(url="getAmount")
    public HashMap<String, Object> getAmount(VMJExchange vmjExchange) {
        Object id = vmjExchange.getGETParam("id");
        ArrayList<String> requiredFields = new ArrayList<>();
        requiredFields.add("amount");

        HashMap<String, Object> hasil = vmjDBUtil.getDataById("financialreport_core", requiredFields, id.toString());

        return hasil;
    }

    @Route(url="getProgram")
    public HashMap<String, Object> getProgram(VMJExchange vmjExchange) {
        Object id = vmjExchange.getGETParam("id");
        ArrayList<String> requiredFields = new ArrayList<>();
        requiredFields.add("idProgram");

        HashMap<String, Object> hasil = vmjDBUtil.getDataById("financialreport_core", requiredFields, id.toString());

        return hasil;
    }

    public abstract HashMap<String, Object> printHeader(VMJExchange vmjExchange);
}