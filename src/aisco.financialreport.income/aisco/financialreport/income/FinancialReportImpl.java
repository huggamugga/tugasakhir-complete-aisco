package aisco.financialreport.income;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

import aisco.financialreport.core.FinancialReportDecorator;
import aisco.financialreport.core.FinancialReportComponent;

@VMJDatabaseTable(tableName = "financialreport_income")
public class FinancialReportImpl extends FinancialReportDecorator {
    public FinancialReportComponent record;

    @VMJDatabaseField(isDelta = true)
    public String paymentMethod;

    @VMJDatabaseField(primaryKey = true, isDelta = true)
    public int idIncome;

    public FinancialReportImpl(FinancialReportComponent record) {
        super(record);
    }

    /**
     * modifies
     */
    /* delta modifies method */
    @Route(url = "printHeader-income")
    public HashMap<String, Object> printHeader(VMJExchange vmjExchange) {
        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("header", "Financial Report - Delta - Income");
        return hasil;
    }

    @Route(url = "sumIncome")
    public HashMap<String, Object> sumIncome(VMJExchange vmjExchange) {
        // String sqlQuery = "select core.amount from financialreport_core as core, financialreport_income as reportIncome where core.id=reportIncome.record_id";

        // VMJDatabaseUtil dbUtil = new VMJDatabaseUtil();

        // int total = 0;

        // ArrayList<Object> incomes = dbUtil.queryForAColumn(sqlQuery, "amount");

        // for (Object income : incomes) {
        //     total += (int) income;
        // }
        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("Total Income", "masih hardcoded");
        return hasil;
    }
}