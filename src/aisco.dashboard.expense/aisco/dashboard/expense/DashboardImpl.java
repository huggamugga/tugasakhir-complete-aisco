package aisco.dashboard.expense;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import aisco.dashboard.core.DashboardDecorator;
import aisco.dashboard.core.DashboardComponent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;

import java.io.IOException;

import vmj.object.mapper.VMJDatabaseUtil;
import vmj.routing.route.VMJExchange;

import vmj.routing.route.Route;

public class DashboardImpl extends DashboardDecorator {
    private DashboardComponent dashboardComponent;
    
    VMJDatabaseUtil dbUtil = new VMJDatabaseUtil();

    public DashboardImpl(DashboardComponent dashboardComponent) {
        super(dashboardComponent);
        this.dashboardComponent = dashboardComponent;
    }

    /**
     * add methods to count totalExpense
     */

    @Route(url="expensetotal")
    public HashMap<String,Object> totalExpense(VMJExchange vmjExchange) {
        String sqlQuery = "select core.amount from financialreport_core as core, financialreport_expense as reportExpense where core.id=reportExpense.record";
        
        int total = 0;

        ArrayList<Object> expenses = dbUtil.queryForAColumn(sqlQuery, "amount");

        for (Object expense : expenses) {
            total += (int) expense;
        }

        HashMap<String,Object> returnObj = new HashMap<>();
        returnObj.put("total_Expense", total);

        return returnObj;
    }


    @Route(url="expensebyid")
    public HashMap<String,Object> totalExpenseByProgramId(VMJExchange vmjExchange) {
        Object id = vmjExchange.getGETParam("id");
        String sqlQuery = "select core.amount from financialreport_core as core, financialreport_expense as reportExpense where core.id=reportExpense.record AND core.idProgram=" + id.toString();
        
        int total = 0;

        ArrayList<Object> expenses = dbUtil.queryForAColumn(sqlQuery, "amount");

        for (Object expense : expenses) {
            total += (int) expense;
        }

        HashMap<String,Object> returnObj = new HashMap<>();
        returnObj.put("total_Expense", total);
        returnObj.put("idProgram", id.toString());

        return returnObj;
    }


    /**
     * modify endpoint method
     */
    @Route(url="partners")
    public List<HashMap<String,Object>> getPartnerDatas(VMJExchange vmjExchange) {
        String sqlQuery = "select progr.partner as partner, count(*) as kali_berpartner from program_core as progr, program_delta_activity as activi where progr.id=activi.program group by partner;";

        ArrayList<String> required = new ArrayList<>();
        required.add("partner");
        required.add("kali_berpartner");

        List<HashMap<String,Object>> hasilQuery = dbUtil.hitDatabaseForQueryATable(sqlQuery, required);

        return hasilQuery;
    }

    @Route(url="printDashboardFromExpense")
    public HashMap<String,Object> printDashboard(VMJExchange vmjExchange) {
        return this.dashboardComponent.printDashboard(vmjExchange);
    }

}