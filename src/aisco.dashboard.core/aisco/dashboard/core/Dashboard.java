package aisco.dashboard.core;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import vmj.routing.route.VMJExchange;


public interface Dashboard {
    HashMap<String,Object> totalIncome(VMJExchange vmjExchange);
    HashMap<String,Object> totalIncomeByProgramId(VMJExchange vmjExchange);
    List<HashMap<String,Object>> getPartnerDatas(VMJExchange vmjExchange);
    HashMap<String,Object> printDashboard(VMJExchange vmjExchange);
}