package aisco.dashboard.core;

import java.util.HashMap;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

public class DashboardImpl extends DashboardComponent {
    public DashboardImpl() {
        super();
    }

    @Route(url = "printDashboard")
    public HashMap<String, Object> printDashboard(VMJExchange vmjExchange) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("dashboard", "core");
        return result;
    }

}