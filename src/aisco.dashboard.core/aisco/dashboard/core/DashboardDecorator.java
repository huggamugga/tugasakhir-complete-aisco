package aisco.dashboard.core;

public abstract class DashboardDecorator extends DashboardComponent {
    public DashboardComponent dashboard = null;
  

    public DashboardDecorator (DashboardComponent dashboard) {
        this.dashboard = dashboard;
    }
}
